# frozen_string_literal: true

require 'spec_helper'
require_relative '../../triage/triage/broken_master_incident_label_validator'
require_relative '../../triage/resources/issue'

RSpec.describe Triage::BrokenMasterIncidentLabelValidator do
  include_context 'with event', 'Triage::IssueEvent' do
    let(:label_names) { [] }

    let(:event_attrs) do
      { label_names: label_names }
    end

    let(:issue_attr) do
      { 'iid' => iid, 'labels' => label_names }
    end

    let(:issue_path) { "/projects/#{project_id}/issues/#{iid}" }
    let(:issue)      { Triage::Issue.new(issue_attr) }
  end

  subject { described_class.new(event) }

  before do
    stub_api_request(path: issue_path, response_body: issue_attr)
  end

  describe '#need_root_cause_label?' do
    context 'when incident has no label' do
      let(:label_names) { [] }

      it 'returns true' do
        expect(subject.need_root_cause_label?).to be true
      end
    end

    context 'when incident has master-broken::undetermined' do
      let(:label_names) { ['master-broken::undetermined'] }

      it 'returns true' do
        expect(subject.need_root_cause_label?).to be true
      end
    end

    context 'when incident has a concrete root cause label such as master-broken::caching' do
      let(:label_names) { ['master-broken::caching'] }

      it 'returns false' do
        expect(subject.need_root_cause_label?).to be false
      end
    end
  end

  describe '#need_flaky_reason_label?' do
    context 'when incident has no label' do
      let(:label_names) { [] }

      it 'returns false' do
        expect(subject.need_flaky_reason_label?).to be false
      end
    end

    context 'when incident has master-broken::undetermined' do
      let(:label_names) { ['master-broken::undetermined'] }

      it 'returns false' do
        expect(subject.need_flaky_reason_label?).to be false
      end
    end

    context 'when incident is caused by something other than flaky-test such as master-broken::caching' do
      let(:label_names) { ['master-broken::caching'] }

      it 'returns false' do
        expect(subject.need_flaky_reason_label?).to be false
      end
    end

    context 'when incident is caused by flaky-test and is already labeled with flaky-test::infrastructure' do
      let(:label_names) { ['master-broken::flaky-test', 'flaky-test::infrastructure'] }

      it 'returns false' do
        expect(subject.need_flaky_reason_label?).to be false
      end
    end

    context 'when incident is caused by flaky test and is missing a flaky reason label' do
      let(:label_names) { ['master-broken::flaky-test'] }

      it 'returns true' do
        expect(subject.need_flaky_reason_label?).to be true
      end
    end
  end
end
