# frozen_string_literal: true

module Triage
  module PipelineFailure
    module Config
      class Base
        DEFAULT_AUTO_TRIAGE_FLAG = false
        DEFAULT_SLACK_ICON = ':boom:'
        DEFAULT_SLACK_USERNAME = GITLAB_BOT
        DEFAULT_SLACK_PAYLOAD_TEMPLATE = <<~JSON
          {
            "blocks": [
              {
                "type": "section",
                "text": {
                  "type": "mrkdwn",
                  "text": "*%<title>s*"
                },
                "accessory": {
                  "type": "button",
                  "text": {
                    "type": "plain_text",
                    "text": "%<incident_button_text>s"
                  },
                  "url": "%<incident_button_link>s"
                }
              },
              {
                "type": "section",
                "text": {
                  "type": "mrkdwn",
                  "text": "*Branch*: %<branch_link>s"
                }
              },
              {
                "type": "section",
                "text": {
                  "type": "mrkdwn",
                  "text": "*Commit*: %<commit_link>s"
                }
              },
              {
                "type": "section",
                "text": {
                  "type": "mrkdwn",
                  "text": "*Triggered by* %<triggered_by_link>s • *Source:* %<source>s • *Duration:* %<pipeline_duration>s minutes"
                }
              },
              {
                "type": "section",
                "text": {
                  "type": "mrkdwn",
                  "text": "*Failed jobs (%<failed_jobs_count>s):* %<failed_jobs_list>s"
                }
              }
              <% if incident %>
              ,{
                "type": "section",
                "text": {
                  "type": "mrkdwn",
                  "text": "*Incident state:* %<incident_state>s • *Incident labels:* %<incident_labels_list>s"
                }
              }
              <% end %>
            ]
          }
        JSON

        def self.match?(event)
          raise NotImplementedError, "#{name} must implement `.#{__method__}`!"
        end

        def initialize(event)
          @event = event
        end

        def create_incident?
          incident_project_id && incident_template
        end

        def incident_project_id
          nil
        end

        def incident_template
          nil
        end

        def incident_labels
          []
        end

        def incident_extra_attrs
          {}
        end

        def slack_channel
          raise NotImplementedError, "#{self.class.name} must implement `##{__method__}`!"
        end

        def slack_username
          DEFAULT_SLACK_USERNAME
        end

        def slack_icon
          DEFAULT_SLACK_ICON
        end

        def slack_payload_template
          DEFAULT_SLACK_PAYLOAD_TEMPLATE
        end

        def slack_options
          {
            channel: slack_channel,
            username: slack_username,
            icon_emoji: slack_icon
          }
        end

        def auto_triage?
          DEFAULT_AUTO_TRIAGE_FLAG
        end

        private

        attr_reader :event
      end
    end
  end
end
