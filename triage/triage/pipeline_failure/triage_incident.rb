# frozen_string_literal: true

require_relative '../../../triage/triage'
require_relative '../../../triage/resources/ci_job'
require_relative '../../../lib/constants/labels'

module Triage
  module PipelineFailure
    class TriageIncident
      ROOT_CAUSE_LABELS = Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS
      LABELS_FOR_TRANSIENT_ERRORS = Labels::MASTER_BROKEN_ROOT_CAUSE_LABELS.values_at(
        :failed_to_pull_image,
        :gitlab_com_overloaded,
        :runner_disk_full,
        :infrastructure
      ).freeze

      ROOT_CAUSE_TO_TRACE_MAP = {
        failed_to_pull_image: ['job failed: failed to pull image'],
        gitlab_com_overloaded: ['gitlab is currently unable to handle this request due to load'],
        runner_disk_full: ['no space left on device'],
        infrastructure: [
          'the requested url returned error: 5', # any 5XX error code should be transient
          'error: downloading artifacts from coordinator',
          'error: uploading artifacts as "archive" to coordinator'
        ]
      }.freeze

      POST_RETRY_JOB_URL_THRESHOLD = 10
      FAILURE_TRACE_SIZE_LIMIT = 50000 # Keep it processable by human

      attr_reader :event, :failed_jobs, :ci_jobs

      def initialize(event, failed_jobs)
        @event = event
        @failed_jobs = failed_jobs
        @ci_jobs = failed_jobs.to_h { |job| [job.id, Triage::CiJob.new(event.project_id, job.id)] }
      end

      def top_root_cause_label
        return ROOT_CAUSE_LABELS[:default] if triaged_jobs.empty?

        # find the top root cause label preferrably not master-broken::undetermined
        potential_labels_root_cause_labels
          .tally
          .max_by { |label, count| label == ROOT_CAUSE_LABELS[:default] ? 0 : count }[0]
      end

      def root_cause_analysis_comment
        return if triaged_jobs.empty?

        [
          list_all_triaged_jobs_comment,
          retry_pipeline_comment,
          close_incident_comment
        ].compact.join("\n\n").prepend("\n\n")
      end

      def investigation_comment
        return if triaged_jobs.empty? || closeable?

        jobs_with_failed_tests = triaged_jobs.reject { |triaged_job| triaged_job[:failed_test_trace].nil? }
        return if jobs_with_failed_tests.empty?

        jobs_with_failed_tests.map do |triaged_job|
          <<~MARKDOWN.chomp
            - #{triaged_job[:link]}:

            #{triaged_job[:failed_test_trace]}
          MARKDOWN
        end.join("\n\n").prepend("\n\n")
      end

      private

      def list_all_triaged_jobs_comment
        return if triaged_jobs.empty?

        triaged_jobs.map do |triaged_job|
          %(- #{triaged_job[:link]}: ~"#{triaged_job[:label]}".#{retry_job_comment(triaged_job)})
        end.join("\n")
      end

      def triaged_jobs
        @triaged_jobs ||=
          failed_jobs.map do |job|
            trace = ci_jobs.fetch(job.id).trace
            root_cause = determine_root_cause_from_trace(trace)

            {
              id: job.id,
              link: "[#{job.name}](#{job.web_url})",
              label: ROOT_CAUSE_LABELS[root_cause],
              failed_test_trace: failed_test_trace(trace)
            }
          end
      end

      def retry_job_comment(failed_job)
        retry_job_web_url = retry_job_and_return_web_url_if_applicable(failed_job)
        return if retry_job_web_url.nil?

        " Retried at: #{retry_job_web_url}"
      end

      def retry_job_and_return_web_url_if_applicable(triaged_job)
        return unless transient_error?(triaged_job[:label]) && retry_jobs_individually?

        ci_jobs.fetch(triaged_job[:id]).retry['web_url']
      end

      def transient_error?(root_cause_label)
        LABELS_FOR_TRANSIENT_ERRORS.include?(root_cause_label)
      end

      def retry_jobs_individually?
        # the benefit of retrying individual job is so we can post the retried job url
        # but if there are too many failed jobs
        # it's more practical to click `retry pipeline` and verify the overall pipeline status when all jobs finish
        failed_jobs.size < POST_RETRY_JOB_URL_THRESHOLD
      end

      def retry_pipeline_comment
        return unless all_jobs_failed_with_transient_errors?
        return if retry_jobs_individually?

        retry_pipeline_response = Triage.api_client.retry_pipeline(event.project_id, event.id).to_h

        "Retried pipeline: #{retry_pipeline_response['web_url']}"
      end

      def close_incident_comment
        return unless closeable?

        "This incident is caused by known transient error(s), closing."
      end

      def all_jobs_failed_with_transient_errors?
        return @all_jobs_failed_with_transient_errors if defined?(@all_jobs_failed_with_transient_errors)

        @all_jobs_failed_with_transient_errors =
          if triaged_jobs.empty?
            false
          else
            (potential_labels_root_cause_labels.uniq - LABELS_FOR_TRANSIENT_ERRORS).empty?
          end
      end
      alias_method :closeable?, :all_jobs_failed_with_transient_errors?
      public :closeable?

      def potential_labels_root_cause_labels
        @potential_labels_root_cause_labels ||= triaged_jobs.map { |job| job[:label] }
      end

      def determine_root_cause_from_trace(trace)
        ROOT_CAUSE_TO_TRACE_MAP.detect do |root_cause, trace_patterns|
          trace_patterns.any? { |pattern| trace.downcase.include?(pattern) }
        end&.first || :default
      end

      def failed_test_trace(trace)
        # RSpec test failure
        # TODO: Add support for Jest
        rspec_failure_summary_start = "Failures:\n"
        rspec_failure_summary_end = "\n[TEST PROF INFO]"

        return unless trace.include?(rspec_failure_summary_start) && trace.include?(rspec_failure_summary_end)

        rspec_trace = trace.split(rspec_failure_summary_start).last.split(rspec_failure_summary_end).first.chomp

        <<~MARKDOWN.chomp
        ```ruby
        #{rspec_trace[...FAILURE_TRACE_SIZE_LIMIT]}
        ```
        MARKDOWN
      end
    end
  end
end
